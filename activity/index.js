// console.log('Hello World');
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
//first function here:
function nagIisangIkaw(){
	let buongPangalan = prompt('What is your full name? ');
	let totoongEdad = prompt('How old are you? ');
	let iyongTirahan = prompt('Where do you live? ');
	alert('Thank you for the information.');
	console.log('Hello ' + buongPangalan);
	console.log('You are ' + totoongEdad + " years old.");
	console.log('You live in ' + iyongTirahan);
};

nagIisangIkaw();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:
function paboritongBanda(){
	let unangPaboritongBanda = '1. EraserHeads';
	let pangalawangPaboritongBanda = '2. Parokya ni Edgar';
	let pangatlongPaboritongBanda = '3. Kamikazee';
	let pangApatnaPaboritongBanda = '4. Rivermaya';
	let pangLimangPaboritongBanda = '5. Sponge Cola';
	console.log(unangPaboritongBanda);
	console.log(pangApatnaPaboritongBanda);
	console.log(pangatlongPaboritongBanda);
	console.log(pangApatnaPaboritongBanda);
	console.log(pangLimangPaboritongBanda);
	// let limangBanda = ['1. EraserHeads', '2. Parokya ni Edgar', '3. Kamikazee', '4. Rivermaya', '5. Sponge Cola'];
	// console.log(limangBanda);
};

paboritongBanda();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function paboritongPelikula(){
	let rottenTomatoesText = 'Rotten Tomatoes rating: '; 
	let unangPelikula = '1. Avengers: Endgame';
	let unangPelikulaRating = '94%'
	let pangalawangPelikula = '2. Shutter Island';
	let pangalawangPelikulaRating = '68%';
	let pangatlongPelikula = '3. The Wolf of Wallstreet';
	let pangatlongPelikulaRating = '80%';
	let pangApatnaPelikula = '4. Inception';
	let pangApatnaPelikulaRating = '87%';
	let pangLimangPelikula = '5. The Pursuit of Happyness'
	let pangLimangPelikulaRating = '67%';
	console.log(unangPelikula);
	console.log(rottenTomatoesText + unangPelikulaRating)
	console.log(pangalawangPelikula);
	console.log(rottenTomatoesText + pangalawangPelikulaRating);
	console.log(pangatlongPelikula);
	console.log(rottenTomatoesText + pangatlongPelikulaRating);
	console.log(pangApatnaPelikula);
	console.log(rottenTomatoesText + pangApatnaPelikulaRating);
	console.log(pangLimangPelikula);
	console.log(rottenTomatoesText + pangLimangPelikulaRating);

};
paboritongPelikula();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);


