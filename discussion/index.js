// console.log('hi');
/*
	functions
		- are lines/block of codes that tell our device/application to perform certain tasks when called/invoked
		- are mostly created to create complicated tasks to run several lines of code in succession
		- they are also used to prevent repeating lines/blocks of code that perform the same task/function

	syntax:
		function functionName(){
			code block (statement)
		}

	>> function keyword
		- used to define a javascript function.
	>> functionName
		- function name. Functions are named to be able to use later in the code.
	>> function block ({})
		- the statements which comprise the body of the function. This is where the code to be executed.
*/

function printName(){
	console.log("My name is Yoby. I am a Software Engineer.");
};

// function invocation - it is common to use the term "call a function" instead of "invoke a function"

printName();

declaredFunction();
// result: err, because it is not yet defined.

function declaredFunction(){
	console.log("This is a defined function.");
};

/* Function Declaration vs Expression
	A function can be created through function declaration by using the function keyword and adding a function name.

	Declared functions are not executed immediately. They are "save for later use", and will be executed later, when they are invoked (called upon)
*/
function declaredFunction2(){
	console.log("Hi! I am from declared function().");
};

declaredFunction2(); //declared functions can be hoisted, as long as the function has been defined.

declaredFunction2();
declaredFunction2();
declaredFunction2();

/*
	function expression
		- a function can also be strored in a variable. This is called a function expression.
		- a function expression is an anonymous function assigned to the variable function.

		anonymous function - function without a name.
*/
let variableFunction = function(){
	console.log("I am from variable function.");
};

variableFunction();
/*
We can also create a function expression of a named function. However, to invoke the function expresson, we invoke it y it's variable name, not by it's function name.

Function expressions are always invoke(called) using the variable name.
*/

let functionExpression = function funcName(){
	console.log("Hello from the other side.");
};

functionExpression();

// You can reassign declared function and function expression to new anonymous function

declaredFunction = function(){
	console.log('Updated declared function.');
};
declaredFunction();

funcExpression = function(){
	console.log('Updated function expression.');
};
funcExpression();

const constantFunction = function(){
	console.log('Initialized with const.');
};

constantFunction();

// constantFunction = function(){
// 	console.log('Cannot be assigned.');
// };
// constantFunction();
// reassignment with const function expression is not possible.


/*
	Function Scoping

	Scope is the accessibility (visibility) of variables within our program. 
		JavaScript variables have 3 types of scope:
		1. Local/block scope
		2. global scope
		3. function scope
*/

{
	let localVar = "Kim Seok-Jin";
}
let globalVar = "The world most handsome";

// console.log(localVar);
// a local variable will be visible only within the function, where it is defined.
console.log(globalVar);
// a global variable has global scope, which means it can be defined anywhere in your JS code.

// Function scope
/*
	JS has a function scope: Each function creates a new scope.
	Variables defined inside a function are not accessible(visible) from outside the function.
	Variable declared var, let and const are quite similar when declared inside a function.
*/

function showName(){
	var functionVar = 'Jungkook';
	const functionConst = "BTS";
	let functionLet = 'kookie';

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showName();
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// Nested function
// You can create another function inside a function. This is called a nested function. This nested function, being inside a new function will have access variable, name as they are within the same scope/code block.

function myNewFunction(){
	let name = "Yor";
	console.log(name);
	function nestedFunction(){
		let nestedName = "Brando";
		console.log(nestedName);
	};
	nestedFunction();
};
myNewFunction();

// Function and Global Scope Variables
// Global Scoped Variable
let globalName = 'Thonie';

function myNewFunction2(){
	let nameInside = 'Kim';
	// Variables declared globally (outside function) have global scope.
	// Global variables can be accessed from anywhere in a JS program including from inside a function.
	console.log(globalName);
	console.log(nameInside);
};

myNewFunction2();

/*
	alert()
		syntax:
			alert('message');
*/ 

alert('Hello World');

function showSampleAlert(){
	alert('Hello user!');
};

showSampleAlert();

console.log("I will only log in in the console when the alert is dismissed.");

/*
prompt()
	syntax:
		prompt('<dialog>');
*/
let samplePrompt = prompt('Enter your name: ');
console.log('Hello ' + samplePrompt);

let sampleNullPrompt = prompt("Don't input anything.");
console.log(sampleNullPrompt);
// if prompt() is cancelled, the result will be "null". However if there no input in the prompt, the result will be "empty string".

function printWelcomeMessage(){
	let firstName = prompt('Enter your first name: ');
	let lastName = prompt('Enter your last name: ');

	console.log('Hello, ' + firstName + " " + lastName + "!");
	console.log("Welcome to Gamer's Guild.");
};

printWelcomeMessage();

// Function naming convention
// 	Function should be definitive of its task. Usually contains a verb

function getCourses(){
	let courses = ['Programming 100', 'Science 101', 'Grammar 102', 'Mathematics 103'];
	console.log(courses);
};

getCourses();
// avoid generic names to avoid confusion
function get(){
	let name = 'Jimmin';
	console.log(name);
};
get();

// Avoid pointless and inappropriate function names
function foo(){
	console.log(25%5);
};

// name function in smallcaps. Follow camelcase when naming functions.

function displayCarInfo(){
	console.log('Brand: Toyota');
	console.log('Type: Sedan');
	console.log('Price: 1,500,000');
};

displayCarInfo(); 